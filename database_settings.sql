create table channels(id int primary key not null,
	server_id int not null, -- foreign key in servers table
	name text not null,
	public int not null,
	enabled int not null
);

create table users(id int primary key not null,
	name text not null,
	password text not null,
	privileges int not null -- 0 - normal user, 1 - moderator, 2 - administrator
);

create table servers(id int primary key not null,
	name text not null,
	host text not null,
	port int not null,
	enabled int not null
);

create table accessors(user_id int not null, -- foreign key in users table
	channel_id int not null -- foreign key in channels table
);
