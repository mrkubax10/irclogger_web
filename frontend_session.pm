# irclogger_web
# Copyright (C) 2023 mrkubax10 <mrkubax10@onet.pl>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package frontend_session;

use Digest::SHA;

use strict;
use warnings;

our %sessions;
my %sessionAccess;

sub newSessionToken {
	my $session = Digest::SHA::sha256_hex(sprintf("%x", rand(0xFFFFFFFF)));
	$sessionAccess{$session} = time();
	return $session;
}

sub deleteSession {
	my $aSession = $_[0];

	if(defined($sessions{$aSession})) {
		delete $sessions{$aSession};
	}
	if(defined($sessionAccess{$aSession})) {
		delete $sessionAccess{$aSession};
	}
}

sub isValidSession {
	my $aSession = $_[0];

	foreach my $key (keys(%sessionAccess)) {
		if(time()-$sessionAccess{$key}>7*24*3600) {
			deleteSession($key);
		}
	}
	if(defined($sessions{$aSession})) {
		$sessionAccess{$aSession} = time();
		return 1;
	}
	return 0;
}

1;
