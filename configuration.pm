# This file is a configuration for irclogger_web. Adjust it for your needs.

package configuration;

our $database = "irclogger.db";
our $logFolder = "logs";
our $botNick = "irclogger__";
our $botPassword = "none";
our $botUsername = "irclogger__";
our $botHostname = "hostname";
our $botName = "Full name of bot";

our $httpServerPort = 8080;

our $verboseLogging = 0;

1;
