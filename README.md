# irclogger_web
irclogger_web is a highly configurable IRC logger with web frontend. It's written in Perl.

## Installation
1. You need to have Perl interpreter and SQLite3 installed.
2. Following Perl packages have to be installed: [DBI](https://metacpan.org/pod/DBI), [DBD::SQLite](https://metacpan.org/pod/DBD::SQLite).
3. Run `./prepare_database.sh` to create SQLite3 database for storing users, servers and logged channels.
4. See `configuration.pm` and adjust it to your needs.
5. Run `perl main.pl` to start HTTP server and logger.
